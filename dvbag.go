// Query the DVB Webservice
package dvbag

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

const (
	serverUrl = "http://widgets.vvo-online.de/abfahrtsmonitor/"
)

// ugly default format 
type verbindung [3]string

// pretty format
type Verbindung struct {
	Linie   string // not an int, can be X or EV11
	Ziel    string
	Ankunft int
}

// your point of interest
type Haltestelle struct {
	Ort  string // e.g. Dresden
	Name string // e.g. Straßburger Platz
}

// Hole Haltestelleninformationen 
func (h Haltestelle) Fetch() ([]Verbindung, error) {
	hst := url.QueryEscape(h.Name)
	ort := url.QueryEscape(h.Ort)
	// XXX wenn das mal nicht gehen sollte, hier ist zB der timestamp etc nicht dabei...
	uri := fmt.Sprintf("%sAbfahrten.do?ort=%s&hst=%s", serverUrl, ort, hst)

	//fmt.Printf("URI %s\n", uri)

	resp, err := http.Get(uri)

	if err != nil {
		return []Verbindung{}, err
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []Verbindung{}, err
	}
	//fmt.Printf("%s\n", data)

	vbslice := new([]verbindung)
	err = json.Unmarshal(data, vbslice)

	if err != nil {
		return []Verbindung{}, err
	}

	//fmt.Printf("%+v\n", vbslice)

	// make pretty
	res := []Verbindung{}
	for _, v := range *vbslice {
		a, _ := strconv.Atoi(v[2])
		t := Verbindung{
			Linie:   v[0],
			Ziel:    v[1],
			Ankunft: a,
		}
		res = append(res, t)
	}

	return res, nil
}
