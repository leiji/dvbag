// testapp for dvbag package
// +build ignore
package main

import (
	"bitbucket.org/leiji/dvbag"
	"fmt"
	"flag"
	"os"
)

var hstVar string
var ortVar string

func init() {
	flag.StringVar(&hstVar, "hst", "Straßburger Platz", "Haltestelle")
	flag.StringVar(&ortVar, "ort", "Dresden", "Ort")

}

func main() {
	flag.Parse()

	h := dvbag.Haltestelle{
		Ort:  ortVar,
		Name: hstVar,
	}
	
	res, err := h.Fetch()

	if err != nil {
		fmt.Printf("ERR %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("\n%10s\t%20s\t%10s\n\n", "Linie", "Ziel", "Ankunft")
	for _, v := range res {
		
		fmt.Printf("%10s\t%20s\t%6d min\n", v.Linie, v.Ziel, v.Ankunft)


	}

}
